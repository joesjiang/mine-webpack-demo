import Vue from "vue";
import App from "./app.vue";
import VueRouter from "vue-router";

const book = r => require.ensure([], () => r(require('./views/book.vue')), 'book')
const music = r => require.ensure([], () => r(require('./views/music.vue')), 'music')
const movie = r => require.ensure([], () => r(require('./views/movie.vue')), 'movie')

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'hash',
  routes: [
    {
      path: '/music',
      component: music
    },
    {
      path: '/movie',
      component: movie
    },
    {
      path: '/book',
      component: book
    }
  ]
})

new Vue({
  el: "#app",
  router: router,
  render: h => h(App)
})